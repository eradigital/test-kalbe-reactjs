let mix = require("laravel-mix");
require("laravel-mix-purgecss");
const path = require("path");
const tailwindcss = require("tailwindcss");

mix
  .sass("source/core/styles/index.scss", "styles/globals.css")
  .setPublicPath("./")
  .options({
    processCssUrls: false,
    postCss: [tailwindcss("./tailwind.config.js")]
  })
  .purgeCss({
    enabled: false,
    extend: {
      content: [
        path.join(__dirname, "source/presentation/**/*.tsx"),
        path.join(__dirname, "source/presentation/**/*.js")
      ],
      whitelistPatterns: [/hljs/]
    }
  });

if (!mix.inProduction()) {
  mix.sourceMaps();
}

if (mix.inProduction()) {
  mix.version();
}
