import { ImageRequestInterface } from "@root-domain/models/image/image_request.model";
import { ImageResponseInterface } from "@root-domain/models/image/image_response.model";
import { uploadImage } from "@root-infrastructure/services/image/upload_image.service";

export const uploadImageRepository = async (data: typeof ImageRequestInterface) => {
  const result: ImageResponseInterface.RootObject = await uploadImage(data);
  return result;
};
