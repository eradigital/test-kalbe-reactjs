import { ImageResponseInterface } from "@root-domain/models/image/image_response.model";
import { deleteImage } from "@root-infrastructure/services/image/delete_image.service";

export const deleteProductRepository = async (id: string) => {
  const result: ImageResponseInterface.RootObject = await deleteImage(id);
  return result;
};
