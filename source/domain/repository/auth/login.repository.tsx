import { LoginResponseInterface } from "@root-domain/models/auth/auth_response.model";
import { LoginRequestInterface } from "@root-domain/models/auth/auth_request.model";
import { login } from '@root-infrastructure/services/auth/login.service';

export const loginRepository = async (data: LoginRequestInterface.RootObject) => {
  const result: LoginResponseInterface.RootObject = await login(data);
  return result;
};
