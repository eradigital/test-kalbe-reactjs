import { LogoutResponseInterface } from "@root-domain/models/auth/logout_response.model";
import { logout } from "@root-infrastructure/services/auth/logout.service";

export const logoutRepository = async () => {
  const result: LogoutResponseInterface.RootObject = await logout();
  return result;
};
