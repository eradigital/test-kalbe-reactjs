export declare module LogoutResponseInterface {
  export interface RootObject {
    success: boolean;
    message: string;
    data: string;
  }
}

export class LogoutResponseModel {
  static toJson(event: LogoutResponseInterface.RootObject) {
    return {
      success: event.success,
      message: event.message,
      data: event.data
    };
  }

  static fromJson(response: any) {
    return {
      success: response.success,
      message: response.message,
      data: response.data
    };
  }
}
