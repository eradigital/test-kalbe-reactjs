import Swal from "sweetalert2";

const alert_notification = ({
  title,
  message,
  status,
  type,
  position = "center"
}: {
  title: string;
  message: string;
  status: string;
  type: string;
  position: any;
}) => {
  let imageUrl: string = "";
  let imageAlt: string = "";

  switch (status) {
    case "success":
      imageUrl += "/images/svg/success.svg";
      imageAlt += "Success";
      break;
    case "info":
      imageUrl += "/images/svg/confirm.svg";
      imageAlt += "Info";
      break;
    case "warning":
      imageUrl += "/images/svg/warning.svg";
      imageAlt += "Warning";
      break;
    case "error":
      imageUrl += "/images/svg/danger.svg";
      imageAlt += "Error";
      break;
    default:
      imageUrl += "/images/svg/success.svg";
      imageAlt += "Success";
      break;
  }

  switch (type) {
    case "confirm":
      return {
        title: title,
        text: message,
        imageUrl: imageUrl,
        imageWidth: 200,
        imageHeight: 100,
        imageAlt: imageAlt,
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ok",
        showClass: {
          popup: "animate__animated animate__fadeInDown"
        },
        hideClass: {
          popup: "animate__animated animate__fadeOutUp"
        }
      };
    default:
      return {
        title: title,
        text: message,
        position: position,
        showConfirmButton: false,
        timer: 3500,
        imageUrl: imageUrl,
        imageWidth: 200,
        imageHeight: 100,
        imageAlt: imageAlt,
        showClass: {
          popup: "animate__animated animate__fadeInDown"
        },
        hideClass: {
          popup: "animate__animated animate__fadeOutUp"
        }
      };
  }
};

export default alert_notification;

export const info_message = (message: any) => {
  let config = alert_notification({
    title: "Info",
    message: message,
    status: "info",
    type: "default",
    position: "center"
  });

  Swal.fire(config);
};

export const success_message = (message: any) => {
  let config = alert_notification({
    title: "Success",
    message: message,
    status: "success",
    type: "default",
    position: "center"
  });

  Swal.fire(config);
};

export const error_message = (message: any) => {
  let config = alert_notification({
    title: "Error",
    message: message,
    status: "error",
    type: "default",
    position: "center"
  });

  Swal.fire(config);
};

export const alert_confirm = (message: any, callBack: () => void) => {
  let config = alert_notification({
    title: "Warning",
    message: message,
    status: "warning",
    type: "confirm",
    position: "center"
  });

  Swal.fire(config).then(function (event) {
    if(event.isConfirmed){
      callBack();
    }
  });
};
