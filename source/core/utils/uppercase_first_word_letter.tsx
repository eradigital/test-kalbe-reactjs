function toUpperCaseFirstLetter(string: string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

export default toUpperCaseFirstLetter;