import { submitFormHandling } from "@root-application/redux/form_handling/actions/submit.action";
import Swal from "sweetalert2";
import alert_notification from "@root-core/utils/alert_notification.util";

export default function onsubmit_form(dispatch: any, stateRedux: any) {
  const stateValidation = stateRedux.form_handling;

  dispatch(submitFormHandling());

  if (stateValidation.formIsValid) {
    return true;
  } else {
    Swal.fire(
      alert_notification({
        title: "Info",
        message: "Mohon isi semua input",
        status: "info",
        type: "default",
        position: "center"
      })
    );
    return false;
  }
}
