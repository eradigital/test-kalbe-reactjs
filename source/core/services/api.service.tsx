import axios from "axios";
import { apiCatchException } from "@root-core/exceptions/api_catch.exception";
import { LocalStorage } from "@root-core/services/local_storage.service";
import { STORAGE } from "@root-core/constants/storage/local.storage";

async function auth() {
  let data: any = await LocalStorage.getDataObjectStorage(STORAGE.AUTH);
  return data;
}

const baseUrl = process.env.REACT_APP_PUBLIC_URL;

export async function postApi(config: any) {
  let key = await auth();
  let token = Object.keys(key).length > 0 ? key.token.access_token : "";
  let data = config.data ? config.data : {};
  let url = config.url ? config.url : "";
  try {
    let config: any = setConfigAxios("POST", url, data, token);
    const response = await axios(config);
    return response.data;
  } catch (error) {
    return apiCatchException(error);
  }
}

export async function getApi(config: any) {
  let key = await auth();
  let token = Object.keys(key).length > 0 ? key.token.access_token : "";
  let data = config.data ? config.data : {};
  let url = config.url ? config.url : "";
  try {
    let config: any = setConfigAxios("GET", url, data, token);
    const response = await axios(config);
    return response.data;
  } catch (error) {
    return apiCatchException(error);
  }
}

export async function putApi(config: any) {
  let key = await auth();
  let token = Object.keys(key).length > 0 ? key.token.access_token : "";
  let data = config.data ? config.data : {};
  let url = config.url ? config.url : "";
  try {
    let config: any = setConfigAxios("PUT", url, data, token);
    const response = await axios(config);
    return response.data;
  } catch (error) {
    return apiCatchException(error);
  }
}

export async function deleteApi(config: any) {
  let key = await auth();
  let token = Object.keys(key).length > 0 ? key.token.access_token : "";
  let data = config.data ? config.data : {};
  let url = config.url ? config.url : "";
  try {
    let config: any = setConfigAxios("DELETE", url, data, token);
    const response = await axios(config);
    return response.data;
  } catch (error) {
    return apiCatchException(error);
  }
}

function setConfigAxios(method: string, url: string, data: any, token: string) {
  if(method === "GET"){
    return {
      method: method,
      url: baseUrl + "/" + url,
      params: data,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    };
  } else {
    return {
      method: method,
      url: baseUrl + "/" + url,
      data: data,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    };
  }
}
