import DotsVerticalIcon from "@root-presentation/components/icons/dots_vertical.icon";
import MessageIcon from "@root-presentation/components/icons/message.icon";
import React from "react";
import Image from "next/image";

function NavbarTemplate() {
  return (
    <div className="fixed z-50 w-full p-3">
      <div className="flex justify-between w-full">
        <div className="overflow-hidden bg-gray-400 rounded-full w-9 h-9">
          <div className="relative h-9 w-9">
            <Image
              src="/images/shared/user.jpeg"
              alt="Picture of the author"
              layout="fill"
              objectFit="cover"
            />
          </div>
        </div>
        <div className="flex">
          <div className="mx-1">
            <MessageIcon />
          </div>
          <div className="mx-1">
            <DotsVerticalIcon />
          </div>
        </div>
      </div>
    </div>
  );
}

export default NavbarTemplate;
