import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
} from 'chart.js';
import { Line } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip,
);

export const options = {
  responsive: true,
  plugins: {
    title: {
      display: true,
      text: '',
    },
  },
};

const labels = ['Jan 2020', 'Feb 2020', 'Mar 2020'];

export const data = {
  labels,

  datasets: [
    {
      data: [0, 20, 60, 50, 45, 50, 60, 70, 40, 45, 35, 25, 30],
      borderColor: 'rgb(255, 99, 132)',
      backgroundColor: 'rgba(255, 99, 132, 0.5)',
    }
  ],
};

export default function LineChartComponent() {
  return <Line options={options} data={data} />;
}
