import React from "react";
import Image from "next/image";

function ItemTargetMissionComponent({
  imageUrl,
  title,
  target,
  reached,
  reachedPercentage,
}: {
  imageUrl: string;
  title: string;
  target: string;
  reached: string;
  reachedPercentage: string;
}) {
  return (
    <div className="flex w-full px-1 py-1 mt-4 overflow-hidden border border-gray-300 rounded-lg">
      <div className="relative w-24 h-24">
        <Image
          src={`/images/home/${imageUrl}`}
          alt="Picture of the author"
          layout="fill"
          objectFit="contain"
        />
      </div>
      <div className="w-full ">
        <div className="flex justify-between w-full p-2">
          <span className="">{title}</span>
          <span className="text-custom-green">Detail</span>
        </div>
        <span className="ml-2 font-bold">
          {reached}
          <span className="font-normal text-gray-500">/{target}</span>
        </span>
        <div className="w-full px-2">
          <div className="w-full h-2 mt-3 bg-gray-300 rounded-full">
            <div
              style={{ width: reachedPercentage + '%'}}
              className="h-2 rounded-full bg-gradient-to-r from-green-400 to-custom-green"
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default ItemTargetMissionComponent;
