import React, { ReactNode } from "react";

function ItemDashboardMenuComponent({
  icon,
  title,
}: {
  icon: ReactNode;
  title: string;
}) {
  return (
    <div className="flex flex-col items-center justify-center w-1/3 my-3 sm:w-1/6">
      {icon}
      <small className="mt-3">{title}</small>
    </div>
  );
}

export default ItemDashboardMenuComponent;
