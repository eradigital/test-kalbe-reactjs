import CalendarIcon from "@root-presentation/components/icons/calendar.icon";
import React from "react";

function DateRangeComponent() {
  return (
    <div className="mx-5 -mt-16 sm:-mt-2">
      <div className="flex items-center justify-center py-2 border border-green-600 rounded-lg">
        <CalendarIcon />
        <span className="ml-2 text-green-600">12/01/2020 - 12/03/2020</span>
      </div>
    </div>
  );
}

export default DateRangeComponent;
