import React from "react";
import DateRangeComponent from "./components/DateRange.component";
import ChartFBOSection from "./sections/ChartFBO.section";
import DashboardMenuSection from "./sections/DashboardMenu.section";
import HeaderHomeSection from "./sections/HeaderHome.section";
import TargetMissionSection from "./sections/TargetMission.section";

function HomeScreen() {
  return (
    <div>
      <HeaderHomeSection />
      <DashboardMenuSection />
      <DateRangeComponent />
      <TargetMissionSection />
      <ChartFBOSection />
    </div>
  );
}

export default HomeScreen;
