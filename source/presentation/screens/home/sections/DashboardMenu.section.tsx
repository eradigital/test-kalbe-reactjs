import CareerIcon from "@root-presentation/components/icons/career.icon";
import React from "react";
import BookIcon from "@root-presentation/components/icons/book.icon";
import AchievementIcon from "@root-presentation/components/icons/achievement.icon";
import DollarIcon from "@root-presentation/components/icons/dollar.icon";
import AnnouncementIcon from "@root-presentation/components/icons/announcement.icon";
import ItemDashboardMenuComponent from "../components/ItemDashboardMenu.component";

function DashboardMenuSection() {
  return (
    <div className="relative -top-20 sm:-top-12">
      <div className="flex flex-wrap px-5 py-2 mx-5 mt-0 bg-white rounded-lg shadow-md justify-evenly">
        <ItemDashboardMenuComponent icon={<CareerIcon />} title={"Career Path"} />
        <ItemDashboardMenuComponent icon={<BookIcon />} title={"Learning Class"} />
        <ItemDashboardMenuComponent icon={<AchievementIcon />} title={"Achievement"} />
        <ItemDashboardMenuComponent icon={<DollarIcon />} title={"Rewards"} />
        <ItemDashboardMenuComponent icon={<AnnouncementIcon />} title={"Announcement"} />
      </div>
    </div>
  );
}

export default DashboardMenuSection;
