import { currency } from "@root-core/utils/currency.util";
import React from "react";
import ItemTargetMissionComponent from "../components/ItemTargetMission.component";

function TargetMissionSection() {
  return (
    <div className="m-5">
      <div className="flex">
        <h6 className="">Target</h6>
        <div className="flex items-center justify-center w-6 h-6 ml-2 bg-yellow-600 rounded-full">
          <span className="text-white">3</span>
        </div>
      </div>
      <div className="sm:grid sm:grid-cols-2 sm:gap-5">
        <ItemTargetMissionComponent
          imageUrl={"phone.webp"}
          title={"Call"}
          target={"12"}
          reached={"48"}
          reachedPercentage={"25"}
        />
        <ItemTargetMissionComponent
          imageUrl={"loop.webp"}
          title={"Store Visibility"}
          target={"10"}
          reached={"8"}
          reachedPercentage={"80"}
        />
        <ItemTargetMissionComponent
          imageUrl={"announcement.webp"}
          title={"NOO"}
          target={"10"}
          reached={"8"}
          reachedPercentage={"80"}
        />
        <ItemTargetMissionComponent
          imageUrl={"arrow.webp"}
          title={"Sales Target"}
          target={currency(200000000)}
          reached={currency(100000000)}
          reachedPercentage={"50"}
        />
      </div>
    </div>
  );
}

export default TargetMissionSection;
