import React from "react";
import LineChartComponent from "../components/LineChart.component";

function ChartFBOSection() {
  return (
    <div className="mx-5 mt-3">
      <span className="text">FBO3X</span>
      <div className="mt-2">
        <LineChartComponent />
      </div>
    </div>
  );
}

export default ChartFBOSection;
