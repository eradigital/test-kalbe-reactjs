import BuildingIcon from "@root-presentation/components/icons/building.icon";
import LocationIcon from "@root-presentation/components/icons/location.icon";
import UserIcon from "@root-presentation/components/icons/user.icon";
import React from "react";
import Image from "next/image";

function HeaderHomeSection() {
  return (
    <div className="relative">
      <div className="w-full h-full bg-gradient-to-r from-green-800 to-green-600 -z-50">
        <div className="relative h-56">
          <Image
            src="/images/home/header_wave.svg"
            alt="Picture of the author"
            layout="fill"
            objectFit="cover"
            objectPosition="-90px -50px"
          />
        </div>
        <div className="absolute top-14">
          <h1 className="m-3 text-xl font-semibold text-white">Welcome back, Adi Susanto!</h1>
          <div className="flex m-2">
            <div className="flex items-center mr-4">
              <UserIcon />
              <span className="ml-2 text-white">Sales</span>
            </div>
            <div className="flex items-center mr-4">
              <BuildingIcon />
              <span className="ml-2 text-white">Sales</span>
            </div>
            <div className="flex items-center mr-4">
              <LocationIcon />
              <span className="ml-2 text-white">Sales</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HeaderHomeSection;
