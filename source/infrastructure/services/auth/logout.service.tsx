import { postApi } from "@root-core/services/api.service";

export const logout = async () => {
  const config = {
    url: "user/logout",
    data: {}
  };
  const response = await postApi(config);
  return response;
};
