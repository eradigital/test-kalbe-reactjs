import { postApi } from "@root-core/services/api.service";

export const login = async (data: any) => {
  const config = {
    url: "user/login",
    data: data
  };
  const response = await postApi(config);
  return response;
};
