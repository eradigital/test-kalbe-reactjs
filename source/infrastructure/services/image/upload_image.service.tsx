import { postApi } from "@root-core/services/api.service";

export const uploadImage = async (data: any) => {
    const config = {
        url: 'image/upload',
        data: data,
    }
    const response = await postApi(config);
    return response;
};
