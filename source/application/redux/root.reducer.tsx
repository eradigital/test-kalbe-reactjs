import { combineReducers } from "redux";

// REDUCER
const rootReducer = combineReducers({});

export default rootReducer;
export type RootState = ReturnType<typeof rootReducer>;
