import {
  CubeTransparentIcon,
  DeviceMobileIcon,
  GlobeAltIcon,
  DesktopComputerIcon,
  SearchCircleIcon,
  ShoppingBagIcon,
  OfficeBuildingIcon,
  TrendingUpIcon,
  UserGroupIcon
} from "@heroicons/react/outline";

export const mainMenu = [
  {
    id: 1,
    name: "Service",
    image: "/images/menu/dashboard.png",
    link: "/service",
    submenu: [
      {
        id: 1,
        link: "/design",
        name: "Design",
        description:
          "Dengan design yg ui/ux yg baik dan interaktif dapat memberikan kemudahan dan kenyamanan pada pengguna.",
        icon: CubeTransparentIcon
      },
      {
        id: 2,
        link: "/mobile",
        name: "Mobile App",
        description:
          "Applikasi mobile app adalah aplikasi yang berjalan pada handphone baik itu android maupun IOS.",
        icon: DeviceMobileIcon
      },
      {
        id: 3,
        link: "/website",
        name: "Website App",
        description:
          "Applikasi website app adalah aplikasi yg berjalan pada web broser seperti google chrome, safari dan banyak lagi.",
        icon: GlobeAltIcon
      },
      {
        id: 4,
        link: "/desktop",
        name: "Desktop App",
        description:
          "Applikasi desktop app adalah aplikasi yg berjalan pada sistem komputer seperti windows ataupun linux.",
        icon: DesktopComputerIcon
      },
      {
        id: 5,
        link: "/seo",
        name: "Search Engine Optimation - SEO",
        description:
          "Search Engine Optimation atau SEO adalah cara untuk membuat website kita ada di pencarian paling atas saat seseorang mencari dari search engine seperti google misalnya.",
        icon: SearchCircleIcon
      }
    ]
  },
  {
    id: 2,
    name: "Sample Feature",
    link: "/feature",
    image: "/images/menu/finance.png",
    submenu: [
      {
        id: 1,
        link: "/ecommerce",
        name: "E-commerce",
        description:
          "E-commerce adalah applikasi penjualan online seperti tokopedia, shopee dan lain-lain.",
        icon: ShoppingBagIcon
      },
      {
        id: 2,
        link: "/company_profile",
        name: "Company Profile",
        description:
          "Compony profile adalah aplikasi yang di gunakan untuk mengenalkan perusahaan kita baik untuk pemasaran maupun personal branding.",
        icon: OfficeBuildingIcon
      },
      {
        id: 4,
        link: "/pos",
        name: "Point Of Sale - POS",
        description:
          "Point Of Sale atau POS adalah aplikasi yang di gunakan untuk management penjualan yang dikenal sebagai aplikasi kasir yang dapat memanage pengelolaan keuangan/buku besar, stok, purchase order dan banyak lagi.",
        icon: TrendingUpIcon
      },
      {
        id: 5,
        link: "/hrms",
        name: "HRMS/HRIS",
        description:
          "Human Resourse Management Sistem/Human Resource Information Sistem merupakan applikasi untuk membantu mengelola bisnis bagian human resource baik itu management karyawan, finance, task, pengarsipan dan banyak lagi tentunya.",
        icon: UserGroupIcon
      }
    ]
  },
  {
    id: 3,
    name: "Gallery",
    image: "/images/menu/finance.png",
    link: "/gallery",
    submenu: []
  },
  {
    id: 4,
    name: "Pricing",
    image: "/images/menu/chat.png",
    link: "/pricing",
    submenu: []
  },
  {
    id: 5,
    name: "Artikel",
    image: "/images/menu/chat.png",
    link: "/blog",
    submenu: []
  },
  {
    id: 6,
    name: "About",
    image: "/images/menu/inbox.png",
    link: "/about",
    submenu: []
  },
  {
    id: 7,
    name: "Consultation",
    image: "/images/menu/project.png",
    link: "/contact",
    submenu: []
  }
];
