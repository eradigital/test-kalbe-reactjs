const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  purge: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./source/presentation/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: false,
  variants: {},
  plugins: [],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Inter var", ...defaultTheme.fontFamily.sans],
      },
      colors: {
        "custom-green": "#09E42C",
      },
    },
  },
};
