const withPlugins = require("next-compose-plugins");
const path = require("path");
const withImages = require("next-images");
const webpack = require("webpack");
module.exports = withPlugins([withImages], {
  reactStrictMode: true,
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")]
  },
  webpack: (config) => {
    config.module.rules.push({
      test: /\.(glb|gltf|bin)$/,
      use: {
        loader: "file-loader"
      }
    });
    config.plugins.push(
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery"
      })
    );
    return config;
  }
});
